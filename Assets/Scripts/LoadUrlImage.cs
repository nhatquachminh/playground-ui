﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadUrlImage : MonoBehaviour
{
    public string[] urlImagesGroup; //= "https://firebasestorage.googleapis.com/v0/b/unityfirebase-test.appspot.com/o/BOY_tall.png?alt=media&token=4400513c-0e86-4a5c-b6c0-d38b1cdc98f7";//"https://picsum.photos/200";
    public Image imageShow;
    public GameObject quadImageShow;
    public Button[] buttonsGroup;

    //private string urlImage;

    // Start is called before the first frame update
    void Start()
    {
        if (urlImagesGroup.Length != buttonsGroup.Length)
        {
            Debug.Log("Recheck Images and Buttons");
        }
        else {
            for (int i = 0; i < buttonsGroup.Length; i++)
            {
                int buttonIndex = i;
                buttonsGroup[buttonIndex].onClick.AddListener(() => OnButtonClicked(buttonIndex)) ;
            }
            //StartCoroutine(GetImage()); 
        }
        
    }

    public void OnButtonClicked(int buttonIndex)
    {
        Debug.Log("Clicked on button " + buttonIndex + " Starting load " + urlImagesGroup[buttonIndex]);
        StartCoroutine(GetImage(urlImagesGroup[buttonIndex])) ;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator GetImage(string url)
    {


        Debug.Log("Getting image");
        
        WWW imageURLWWW = new WWW(url);

        yield return imageURLWWW;
        if (imageURLWWW.texture != null)
        {
            Debug.Log("Got texture: " + imageURLWWW.texture.width + " - " + imageURLWWW.texture.height);
            Sprite sprite;
            int imgWidth = imageURLWWW.texture.width;
            int imgHeight = imageURLWWW.texture.height;

            sprite = Sprite.Create(imageURLWWW.texture, new Rect(0, 0, imgWidth, imgHeight), Vector2.zero);

            var img = imageShow.transform as RectTransform;
            img.sizeDelta = new Vector2(imgWidth, imgHeight);

            float iWidth = (float)imgWidth / 100;
            float iHeight = (float)imgHeight / 100;
            quadImageShow.transform.localScale = new Vector3(iWidth, iHeight,1);

            quadImageShow.GetComponent<MeshRenderer>().material.mainTexture = imageURLWWW.texture;
            imageShow.sprite = sprite;
        }

    }
}
