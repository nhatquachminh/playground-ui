﻿using UnityEngine;
using UnityEngine.UI;

public class CallNumberTest : MonoBehaviour
{
    public InputField inputPhoneNumber;
    public Button btnDial;
    public Text alertText;

    // Start is called before the first frame update
    void Start()
    {
        inputPhoneNumber.text = null;
        btnDial.onClick.AddListener(()=> CheckInput(inputPhoneNumber.text)); //lamda function to convert from UnityAction to void
        alertText.gameObject.SetActive(false); //hide alert text on startup.
    }

    // Update is called once per frame
    void Update()
    {
        //auto clear phone input when click. Comment this if you don't want to use.
        //Still not worked yet, totally forgot how i did this before.
        /*if (inputPhoneNumber.isFocused)
        {
            inputPhoneNumber.text = "";
        }*/
    }

    void CheckInput(string stringPhoneNumber)
    {
        //checked input field is empty or not
        if (inputPhoneNumber.text == "" || inputPhoneNumber.text == null)
        {
            alertText.text = "Please enter a phone number before call";
            alertText.gameObject.SetActive(true);
        }
        else
        {
            OnDial(stringPhoneNumber);
            alertText.gameObject.SetActive(false);
        }
    }

    void OnDial(string stringPhoneNumber)
    {
        //check input string is corrected interger or not, even i was set inputField is Integer input only. Double check !!!
        int pNumber;
        bool isNumeric = int.TryParse(stringPhoneNumber, out pNumber) ;

        if (isNumeric == true)
        {
            alertText.gameObject.SetActive(false);
            Debug.Log("PhoneNo is interger: " + pNumber);
            Application.OpenURL("tel://0" + pNumber); 
            //this will open a popup request to use call app in your phone. 
            //Doesn't work on my PC, if your PC have an app to call, it may be work. Please let me khow, thanks.
        }
        else
        {
            Debug.Log("<color=red>PhoneNo isn't interger, somehow !!!</color>");
            alertText.text = "Please recheck your phone number";
            alertText.gameObject.SetActive(true);
        }
    }
}
